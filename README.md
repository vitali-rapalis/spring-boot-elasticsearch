[![Java Version](https://img.shields.io/badge/Java-21-blue)](https://www.oracle.com/java/technologies/javase/jdk21-archive-downloads.html)
[![Spring Version](https://img.shields.io/badge/Spring-3.2.3-brightgreen)](https://docs.spring.io/spring-boot/docs/current/reference/html)

# Spring Boot Elasticsearch

---

> **Elasticsearch** integration with **spring boot** example. In this example, the jokes from 
> [**chuck norris joke api**](https://api.chucknorris.io/#!) will be persisted to elasticsearch, for further full text analysis.
> Via [**swagger ui**](http://localhost:8080/swagger-ui/index.html), import of jokes and full phrase analysis search
> could be done.

## How To Run

This command will start external dependencies (**externalsearch, kibana**) via docker-compose plugin
and starts the application.

``
./gradlew bootRun
``

## Screenshots

Import of jokes from chuck norris api, jokes count can be set.

![Import](docs/images/import.png "Import of jokes from chuck norris api")

Result in kibana after import.

![Kibana](docs/images/kibana.png "Result in kibana")

Full phrase test search
with [height-lighting](https://www.elastic.co/guide/en/elasticsearch/reference/current/highlighting.html)
and [fuzzy](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-fuzzy-query.html) in joke
documents.

![Search](docs/images/search.png "Search phrase in jokes")

## Links

- [Kibana](http://localhost:5601/app/dev_tools#/console)
- [Elasticsearch](http://localhost:9200)
- [Swagger UI](http://localhost:8080/swagger-ui/index.html)
    - [Import Jokes](http://localhost:8080/swagger-ui/index.html#/Joke/importJokes)
    - [Full Jokes Phrase Search](http://localhost:8080/swagger-ui/index.html#/Joke/find)                                

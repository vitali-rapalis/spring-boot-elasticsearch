package com.vrapalis.springbootelasticsearch.domain.joke;

import java.util.List;
import java.util.Map;

public record JokeFoundDto(String id, String title, String body,
                           Map<String, List<String>> highlight) {
}

package com.vrapalis.springbootelasticsearch.domain.joke;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.Optional;
import java.util.List;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Joke", description = "The jokes can be created and searched")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/jokes")
public class JokeRestController {

  private final JokeService service;


  @PostMapping
  @Operation(description = "Create joke", summary = "Joke will be created, and persisted to elasticsearch")
  public void post(@NonNull @RequestBody final JokeDocument author) {
    service.save(author);
  }

  @PostMapping("import")
  @Operation(description = "Import jokes", summary = "Import jokes, from Chuck Norris API https://api.chucknorris.io/#!")
  public ResponseEntity importJokes(@Parameter(description = "Count jokes", example = "10",
      allowEmptyValue = true) @NonNull final @RequestParam(required = false)
                                    Optional<Integer> count) {
    return service.importJoke(count);
  }

  @PostMapping("find")
  @Operation(description = "Find jokes", summary = "Find jokes by specified criteria")
  public ResponseEntity<List<JokeFoundDto>> find(@Parameter(description = "Count jokes", example = "Chuck Norris")
      @NonNull final @RequestBody JokeFindDto findRequest) {
    return service.find(findRequest);
  }


  @GetMapping(value = {"", "{id}"})
  @Operation(description = "Get jokes", summary = "Get all jokes, or one by id from elasticsearch")
  public ResponseEntity<List<JokeDocument>> get(
      @Parameter(description = "Joke id", example = "21be7484-3913-410b-a075-aea9e9e5d891")
      @PathVariable(required = false) Optional<String> id) {
    return service.findOne(id);
  }

  @DeleteMapping("{id}")
  @Operation(description = "Delete joke", summary = "Delete joke from elasticsearch")
  public void delete(
      @Parameter(description = "Joke id", example = "21be7484-3913-410b-a075-aea9e9e5d891")
      @NonNull final @PathVariable String id) {
    service.delete(id);
  }
}

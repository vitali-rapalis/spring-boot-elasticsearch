package com.vrapalis.springbootelasticsearch.domain.joke;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JokeRepository extends ElasticsearchRepository<JokeDocument, String> {
}

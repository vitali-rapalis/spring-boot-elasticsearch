package com.vrapalis.springbootelasticsearch.domain.joke;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@Configuration
@ConfigurationProperties("joke")
public class JokeProperties {
  @NotNull
  private Integer count = 10;

  @NotBlank
  private String chuckNorrisApi = "https://api.chucknorris.io/jokes/random";
}

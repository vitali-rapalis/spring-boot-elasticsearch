package com.vrapalis.springbootelasticsearch.domain.joke;

import co.elastic.clients.elasticsearch._types.query_dsl.Operator;
import co.elastic.clients.elasticsearch._types.query_dsl.QueryBuilders;
import co.elastic.clients.elasticsearch._types.query_dsl.TextQueryType;
import java.net.URI;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Function;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.client.elc.NativeQuery;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.query.HighlightQuery;
import org.springframework.data.elasticsearch.core.query.highlight.Highlight;
import org.springframework.data.elasticsearch.core.query.highlight.HighlightField;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;

@Log4j2
@Service
@RequiredArgsConstructor
public class JokeServiceImpl implements JokeService {

  private final WebClient webClient;
  private final JokeProperties properties;
  private final JokeRepository repository;
  private final ElasticsearchOperations operations;


  @Override
  public void save(@NonNull final JokeDocument jokeDocument) {
    repository.save(jokeDocument);
    log.debug("User with id: {} was persisted to es", jokeDocument.getId());
  }

  @Override
  public ResponseEntity<List<JokeDocument>> findOne(@NonNull final Optional<String> id) {
    return id.flatMap(i -> repository.findById(i)).map(author -> ResponseEntity.ok(List.of(author)))
        .orElse(ResponseEntity.ok(Collections.emptyList()));
  }

  @Override
  public void delete(@NonNull final String id) {
    repository.deleteById(id);
    log.debug("User with id: {} was deleted", id);
  }

  @Override
  @SneakyThrows
  public ResponseEntity importJoke(@NonNull final Optional<Integer> count) {
    val jokesCount = count.orElse(properties.getCount());

    callChuckNorrisApi(jokesCount)
        .parallel()
        .runOn(Schedulers.fromExecutor(Executors.newVirtualThreadPerTaskExecutor()))
        .map(mapToJockDocument())
        .doOnNext(persistToElasticsearch())
        .sequential()
        .blockLast();

    log.debug("Jokes from chuck norris api: {} ware imported", properties.getChuckNorrisApi());
    return ResponseEntity.created(new URI("/api/authors")).build();
  }

  @Override
  public ResponseEntity<List<JokeFoundDto>> find(@NonNull JokeFindDto findRequest) {
    val highlight = new HighlightQuery(new Highlight(List.of(new HighlightField("title"),
        new HighlightField("body"))), JokeDocument.class);

    val query = NativeQuery.builder()
        .withQuery(QueryBuilders.multiMatch(builder -> builder
            .fields(List.of("title", "body"))
            .query(findRequest.phrase())
            .type(TextQueryType.BestFields)
            .fuzziness("1")
            .operator(Operator.Or)))
        .withHighlightQuery(highlight)
        .withPageable(Pageable.unpaged())
        .build();

    var jokes = operations.search(query, JokeDocument.class)
        .get()
        .parallel()
        .map(mapHitToFoundDto())
        .toList();

    return ResponseEntity.ok(jokes);
  }

  private static Function<SearchHit<JokeDocument>, JokeFoundDto> mapHitToFoundDto() {
    return hit -> new JokeFoundDto(hit.getContent().getId(), hit.getContent().getTitle(),
        hit.getContent().getBody(), hit.getHighlightFields());
  }

  private Function<JokeChuckNorrisDto, JokeDocument> mapToJockDocument() {
    return dto -> new JokeDocument(dto.id(), splitToTitle(dto.value()), dto.value());
  }

  private static String splitToTitle(String text) {
    return Arrays.stream(text.split(" ")).limit(3).reduce("", (s, s2) -> (s + " " + s2).trim());
  }

  private Consumer<JokeDocument> persistToElasticsearch() {
    return document -> repository.save(document);
  }

  private Flux<JokeChuckNorrisDto> callChuckNorrisApi(Integer count) {
    return Flux.range(1, count)
        .flatMap(c -> webClient.get()
            .uri(properties.getChuckNorrisApi())
            .retrieve()
            .bodyToMono(JokeChuckNorrisDto.class));
  }
}

package com.vrapalis.springbootelasticsearch.domain.joke;

public record JokeFindDto(String phrase) {
}

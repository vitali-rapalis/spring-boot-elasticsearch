package com.vrapalis.springbootelasticsearch.domain.joke;

import java.util.List;

public record JokeChuckNorrisDto(String id, String value, List<String> categories) {
}

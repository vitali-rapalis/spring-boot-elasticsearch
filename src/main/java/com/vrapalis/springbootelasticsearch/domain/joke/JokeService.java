package com.vrapalis.springbootelasticsearch.domain.joke;

import java.util.Optional;
import java.util.List;
import lombok.NonNull;
import org.springframework.http.ResponseEntity;

public interface JokeService {

  void save(@NonNull final JokeDocument jokeDocument);

  ResponseEntity<List<JokeDocument>> findOne(@NonNull final Optional<String> id);

  void delete(@NonNull final String id);

  ResponseEntity importJoke(@NonNull final Optional<Integer> count);

  ResponseEntity<List<JokeFoundDto>> find(@NonNull final JokeFindDto findRequest);
}
